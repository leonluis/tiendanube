
output "ec2_sg_id" {
  value = aws_security_group.ec2.id
}

output "lb_sg_id" {
  value = aws_security_group.lb.id
}

