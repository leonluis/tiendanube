variable ec2_sg_name {
  default     = "ec2-sg"
  description = "The name of the ec2 security group."
}

variable lb_sg_name {
  default     = "lb-sg"
  description = "The name of the alb security group."
}

variable vpc_id {
  description = "The VPC ID."
}
