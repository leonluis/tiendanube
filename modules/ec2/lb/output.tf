output "id" {
  value = aws_lb.test.id
}

output "dns" {
  value = aws_lb.test.dns_name
}