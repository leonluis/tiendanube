# application load balancer 

resource "aws_lb" "test" {
  name                       = var.name
  internal                   = var.is_internal
  load_balancer_type         = var.load_balancer_type
  security_groups            = var.security_groups
  subnets                    = var.subnets
  enable_deletion_protection = var.enable_deletion_protection

  tags = {
    Environment = "test"
  }
}

# web servers target groups

resource "aws_alb_target_group" "nginx" {
  name     = "test-alb-nginx-target-group"
  port     = 80
  protocol = "HTTP"
  vpc_id   = var.vpc_id
}

resource "aws_alb_target_group" "apache2" {
  name     = "test-alb-apache2-target-group"
  port     = 80
  protocol = "HTTP"
  vpc_id   = var.vpc_id
}

# alb target group attachments

resource "aws_lb_target_group_attachment" "nginx" {
  target_group_arn = aws_alb_target_group.nginx.arn
  target_id        = lookup(var.ec2_instance_ids, "nginx")
  port             = 80
}

resource "aws_lb_target_group_attachment" "apache2" {
  target_group_arn = aws_alb_target_group.apache2.arn
  target_id        = lookup(var.ec2_instance_ids, "apache2")
  port             = 80
}

# alb listener rule

resource "aws_alb_listener" "ec2" {
  load_balancer_arn = aws_lb.test.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "forward"

    forward {

      target_group {
        arn    = aws_alb_target_group.nginx.arn
        weight = 50
      }

      target_group {
        arn    = aws_alb_target_group.apache2.arn
        weight = 50
      }

      stickiness {
        enabled  = false
        duration = 60
      }

    }
  }
}
