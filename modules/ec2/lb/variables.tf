
variable vpc_id {
  description = "VPC id used in lb target groups."
}

variable subnets {
  description = "A list of subnet IDs to attach to the LB."
}

variable ec2_instance_ids {
  type        = map
  description = "These are the Instance IDs to use in LB group attachments."
}

variable security_groups {
  type        = list
  description = "A list of security group IDs to assign to the LB."
}

variable name {
  default     = "test-lb-tf"
  description = "The name of the LB."
}

variable is_internal {
  default = false
}

variable load_balancer_type {
  default     = "application"
  description = "The type of load balancer to create."
}

variable enable_deletion_protection {
  default     = false
  description = "This prevents Terraform from deleting the load balancer if set true."
}
