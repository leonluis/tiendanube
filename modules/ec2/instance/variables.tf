variable ami {
  # ami-038e35de01603d84e
  default     = "ami-0947d2ba12ee1ff75"
  description = "The ID of the AMI used to launch the instance."
}

variable instance_type {
  default     = "t2.micro"
  description = "The type of the Instance."
}

variable key_name {
  default     = "staging-temporary-keypair"
  description = "The key name of the Instance."
}

variable subnet_id {
  description = "Private VPC subnet ID."
}

variable user_data {
  type        = map
  description = "User Data supplied to the Instances."
}

variable vpc_security_group_ids {
  description = "The associated security groups in a non-default VPC."
}
