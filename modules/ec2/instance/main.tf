
resource "aws_instance" "nginx" {
  ami                    = var.ami
  instance_type          = var.instance_type
  key_name               = aws_key_pair.generated_key.key_name
  subnet_id              = var.subnet_id
  user_data              = lookup(var.user_data, "nginx")
  vpc_security_group_ids = var.vpc_security_group_ids

  tags = {
    Name        = "nginx"
    Environment = "test"
    Account     = "tiendanube"
  }
}

resource "aws_instance" "apache2" {
  ami                    = var.ami
  instance_type          = var.instance_type
  key_name               = aws_key_pair.generated_key.key_name
  subnet_id              = var.subnet_id
  user_data              = lookup(var.user_data, "apache2")
  vpc_security_group_ids = var.vpc_security_group_ids

  tags = {
    Name        = "apache2"
    Environment = "test"
    Account     = "tiendanube"
  }
}

# This use case is only for the given technical challenge
resource "tls_private_key" "temporary" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "generated_key" {
  key_name   = var.key_name
  public_key = tls_private_key.temporary.public_key_openssh
}
