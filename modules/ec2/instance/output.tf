output "nginx_instance_id" {
  value = aws_instance.nginx.id
}

output "apache2_instance_id" {
  value = aws_instance.apache2.id
}

output "nginx_instance_private_ip" {
  value = aws_instance.nginx.private_ip
}

output "apache2_instance_private_ip" {
  value = aws_instance.apache2.private_ip
}

output "private_key_pem" {
  value = tls_private_key.temporary.private_key_pem
}

output "public_key_openssh" {
  value = tls_private_key.temporary.public_key_pem
}