output "id" {
  value       = aws_vpc.test.id
  description = "VPC ID"
}

output "cidr_block" {
  value       = var.cidr_block
  description = "The CIDR block associated with the VPC"
}

output "owner_id" {
  value = aws_vpc.test.owner_id
}

output "private_subnet_id" {
  value = aws_subnet.private.id
}

output "public_subnets_ids" {
  value = [aws_subnet.public_1.id, aws_subnet.public_2.id]
}
