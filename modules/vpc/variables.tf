variable cidr_block {
  default     = "10.0.0.0/16"
  description = "The CIDR block for the VPC."
}

variable enable_dns_support {
  default     = true
  description = "A boolean flag to enable/disable DNS support in the VPC."
}

variable enable_dns_hostnames {
  default     = false
  description = "A boolean flag to enable/disable DNS hostnames in the VPC."
}

variable instance_tenancy {
  default     = "default"
  description = "Tenancy of instances spin up within VPC."
}

variable availability_zone {
  default = "us-east-1"
}

variable subnet_data {
  type        = map
  description = "Subnets CIDR blocks and AZs to use when creating subnets."
}
