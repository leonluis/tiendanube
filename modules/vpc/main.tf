resource "aws_vpc" "test" {
  cidr_block           = var.cidr_block
  enable_dns_support   = var.enable_dns_support
  enable_dns_hostnames = var.enable_dns_hostnames
  instance_tenancy     = var.instance_tenancy

  tags = {
    Account     = "tiendanube"
    Name        = "test-vpc"
    Environment = "test"
  }
}

# elastic ip for the natgateway

resource "aws_eip" "eip" {
  vpc = true

  tags = {
    Name = "test-nat-gateway-eip"
  }
}

# nat gateway for private subnet

resource "aws_nat_gateway" "test_ngw" {
  allocation_id = aws_eip.eip.id
  subnet_id     = aws_subnet.public_1.id
}

# internet gateway for public subnet

resource "aws_internet_gateway" "test_igw" {
  vpc_id = aws_vpc.test.id
  tags = {
    Environment = "test"
  }
}

# private and public subnets

resource "aws_subnet" "private" {
  availability_zone       = lookup(var.subnet_data, "private_az")
  cidr_block              = lookup(var.subnet_data, "private_cidr")
  map_public_ip_on_launch = "false"
  vpc_id                  = aws_vpc.test.id

  tags = {
    Name        = "test-private-subnet"
    Environment = "test"
  }
}

resource "aws_subnet" "public_1" {
  availability_zone       = lookup(var.subnet_data, "public_az_1")
  cidr_block              = lookup(var.subnet_data, "public_cidr_1")
  map_public_ip_on_launch = "false"
  vpc_id                  = aws_vpc.test.id

  tags = {
    Name        = "test-public-subnet"
    Environment = "test"
  }
}

resource "aws_subnet" "public_2" {
  availability_zone       = lookup(var.subnet_data, "public_az_2")
  cidr_block              = lookup(var.subnet_data, "public_cidr_2")
  map_public_ip_on_launch = "false"
  vpc_id                  = aws_vpc.test.id

  tags = {
    Name        = "test-public-subnet"
    Environment = "test"
  }
}

# private and public route tables

resource "aws_route_table" "private" {
  vpc_id = aws_vpc.test.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.test_ngw.id
  }

  tags = {
    Name        = "private-route-table"
    Environment = "test"
  }
}

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.test.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.test_igw.id
  }

  tags = {
    Name        = "public-route-table"
    Environment = "test"
  }
}

# route table associations

resource "aws_route_table_association" "private-subnet" {
  subnet_id      = aws_subnet.private.id
  route_table_id = aws_route_table.private.id
}

resource "aws_route_table_association" "public-subnet-1" {
  subnet_id      = aws_subnet.public_1.id
  route_table_id = aws_route_table.public.id
}

resource "aws_route_table_association" "public-subnet-2" {
  subnet_id      = aws_subnet.public_2.id
  route_table_id = aws_route_table.public.id
}
