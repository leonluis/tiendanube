#!/bin/bash

apt-get update && apt-get install apache2 -y
service apache2 enable && service apache2 start
ufw allow 'Apache'
echo "<b>APACHE</b> RESPONSE" > /var/www/html/index.html