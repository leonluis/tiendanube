#!/bin/bash

apt-get update && apt-get install nginx -y
service nginx enable && service nginx start
ufw allow 'Nginx HTTP'
echo "<b>NGINX</b> RESPONSE" > /var/www/html/index.nginx-debian.html