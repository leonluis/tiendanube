provider "aws" {
  region     = var.aws_region
  profile    = var.aws_profile
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}

module "vpc" {
  source               = "../modules/vpc"
  cidr_block           = "10.0.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = false
  instance_tenancy     = "default"

  subnet_data = {

    "private_cidr" = "10.0.1.0/24"
    "private_az"   = "us-east-1a"

    "public_cidr_1" = "10.0.2.0/24"
    "public_cidr_2" = "10.0.3.0/24"
    "public_az_1"   = "us-east-1a"
    "public_az_2"   = "us-east-1b"

  }
}

module "sg" {
  source      = "../modules/ec2/sg"
  ec2_sg_name = "ec2-sg"
  lb_sg_name  = "lb-sg"
  vpc_id      = module.vpc.id
}

module "instance" {
  source    = "../modules/ec2/instance"
  ami       = "ami-0dba2cb6798deb6d8"
  key_name  = "temporary_keypair"
  subnet_id = module.vpc.private_subnet_id

  vpc_security_group_ids = [module.sg.ec2_sg_id]

  user_data = {
    "nginx"   = file("../userdata/install_nginx.sh"),
    "apache2" = file("../userdata/install_apache2.sh")
  }
}

module "lb" {
  source                     = "../modules/ec2/lb"
  vpc_id                     = module.vpc.id
  subnets                    = module.vpc.public_subnets_ids
  security_groups            = [module.sg.lb_sg_id]
  name                       = "tiendanube-test-lb"
  is_internal                = false
  load_balancer_type         = "application"
  enable_deletion_protection = false

  ec2_instance_ids = {
    "nginx"   = module.instance.nginx_instance_id
    "apache2" = module.instance.apache2_instance_id
  }
}
