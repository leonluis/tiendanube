
output "private_key" {
  value = module.instance.private_key_pem
}

output "public_key" {
  value = module.instance.public_key_openssh
}

output "lb_address" {
  value = module.lb.dns
}