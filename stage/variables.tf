variable "aws_access_key" {
  default = null
}

variable "aws_secret_key" {
  default = null
}

variable "aws_profile" {
  default = null
}

variable "aws_region" {
  default = null
}
