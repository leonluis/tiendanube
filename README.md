<img src="https://d26lpennugtm8s.cloudfront.net/assets/insti/img/json-ld-tiendanube.png"
     alt="Tienda Nube logo"
     width="250" height="50" />
     
# Terraform technical challenge

## Pre-Requisites
* Terraform installed
* AWS credentials configured using aws cli or via export
    ```
    aws configure
    ```

## The Test

```
You need to create a stack using infrastructure as a code with terraform and this structure needs to be built 
into AWS provider using t2.micro instances (aws free tier - we aren'tn responsible for AWS fees), or using 
yourself criterias over your aws account.

The resources:

1x Application Load Balancer
2x EC2 Server

Server 01 - must have Nginx.
Server 02 - must have Apache.

2x security groups
To ALB
To EC2 instances.

Besides that, all resources involving ALB stack must be created too. Important: All resources need to be created using Terraform.

Deliverable
The terraform script using some repository in github, bitbucket or other.
Terraform output needs to expose ALB address.
```

Information
-----

This repository contains a modular approach to the creation of the following AWS resources (24 in total):

- VPC
- Subnets
- Route tables
- Route table associations
- EC2 instances
- Application Load Balancer
- ALB Listener
- ALB Target groups
- ALB Target group attachments
- Elastic IP
- Security groups
- Internet Gateway
- Nat Gateway
- Key Pair
- TLS private Key

Each one of the modules contains specific variables that provide a flexible approach to customize infrastructure values.

Note that this is meant to be run in a local, controlled environment. Please check your AWS credentials file and make sure you are using the correct profile.

Example:
```
module "vpc" {
  source               = "../modules/vpc"
  cidr_block           = "10.0.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = false
  instance_tenancy     = "default"
}
```

## Usage

1. Clone this repository and change to Terraform directory

    ```
    git clone git@gitlab.com:leonluis/tiendanube.git && cd ./tiendanube
    ```

2. Update variables.tf or export them to reflect your desired values. For example:
    ```
    export TF_VAR_aws_profile=aws_profile
    export TF_VAR_aws_region=aws_region
    export TF_VAR_aws_access_key=access_key
    export TF_VAR_aws_secret_key=secret_key

    ```
    Valid .aws/credentials profile example:
    ```
    [tiendanube]
    region = us-east-1
    aws_access_key_id = XXXXXXXXXX
    aws_secret_access_key = XXXXXXXXXX    
    ```
    Note that the selected profile must have sufficient permissions to create the stated resources
   
3. Initialize Terraform
    ```
    terraform init
    ```
4. Run Terraform Plan, 24 new resources should be created
    ```
    terraform plan -out TiendaNube
    ```  
5. Run Terraform Apply
    ```
    terraform apply "TiendaNube"
    ```

## Verification

1. The ALB address will be shown once the Apply command finishes, otherwise you can run:
    ```
    terraform output lb_address
    ```

2. Access the ALB address using your browser in order to get responses from the configured target groups

3. Verify the created resources using AWS Console

   Note: a random key pair is generated when executing this script and shown once the Apply command finishes, you can also inspect the terraform.tfstate file.
   However, no ingress rules are created in order allow traffic through port 22.

## Clean-Up

1. Run Terraform destroy to remove all the previously created resources

    ```
    terraform destroy
    ```

## Additional thoughts

1. Both web servers can be run using Docker instead
2. There are "already made" modules that provide the functionality we need in order to spin up these resources
   
   ref: https://registry.terraform.io/namespaces/terraform-aws-modules
  
    ```
    module "vpc" {
      source = "terraform-aws-modules/vpc/aws"

      name = "tiendanube"
      cidr = "10.0.0.0/16"

      azs             = ["eu-east-1a", "eu-east-1b", "eu-east-1c"]
      private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
      public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]

      enable_nat_gateway = true
      enable_vpn_gateway = true

      tags = {
        Terraform = "true"
        Environment = "test"
      }
    }
    ```